<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            'descripcion'=>'pelota',
            'id_departamento'=>1,
            'precio'=>12.21
            ]);   

            DB::table('productos')->insert([
                'descripcion'=>'playera',
                'id_departamento'=>1,
                'precio'=>123.54
                ]);   
             
             DB::table('productos')->insert([
                'descripcion'=>'tennis',
                'id_departamento'=>1,
                'precio'=>231.90
                ]);   
         }

       
         
    }

