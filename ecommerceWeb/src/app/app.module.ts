import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardsComponent } from './components/cards/cards.component';
import { LoginComponent } from './view/login/login.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { WebpageComponent } from './layouts/webpage/webpage.component';

import {JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { CartComponent } from './view/cart/cart.component';
import { PaidComponent } from './view/paid/paid.component';
import { ProductosComponent } from './view/productos/productos.component';
import { ComprasComponent } from './view/compras/compras.component';
import { VentasComponent } from './view/ventas/ventas.component';

@NgModule({
  declarations: [
    AppComponent,
    CardsComponent,
    LoginComponent,
    AuthComponent,
    WebpageComponent,
    CartComponent,
    PaidComponent,
    ProductosComponent,
    ComprasComponent,
    VentasComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [JwtHelperService, {provide:JWT_OPTIONS, useValue: JWT_OPTIONS}],
  bootstrap: [AppComponent]
})
export class AppModule { }
