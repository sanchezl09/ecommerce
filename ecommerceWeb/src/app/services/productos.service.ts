import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  baseUrl = environment.appUrl;

  constructor(private httpclient : HttpClient) { }

  getProductos(){
    return this.httpclient.get(`${this.baseUrl}/productos`);
  }

  storeProducto(data:any)
  {
    return this.httpclient.post(`${this.baseUrl}/productos`,data).subscribe();
  }

  showProductSelect(id:any){
    return this.httpclient.get(`${this.baseUrl}/productos/view/${id}`);
  }
  storeProdcutEdit(id:any, data:any){
    return this.httpclient.put(`${this.baseUrl}/productos/${id}`,data).subscribe();

  }

  deleteProducto(id:any){
return this.httpclient.delete(`${this.baseUrl}/productos/${id}`).subscribe()
  }
}
