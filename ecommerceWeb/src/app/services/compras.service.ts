import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class ComprasService {
  baseUrl = environment.appUrl;

  constructor(private http: HttpClient) { }


  getCompras(id:any)
  {
  return this.http.get(`${this.baseUrl}/compras/${id}`)
  }

  getAllCompras()
  {
    return this.http.get(`${this.baseUrl}/compras`);
  }
}
