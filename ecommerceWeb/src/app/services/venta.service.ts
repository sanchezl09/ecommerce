import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class VentaService {
  baseUrl = environment.appUrl;

  constructor(private http:HttpClient, private router: Router) { }
  saveVenta(data:any){
    this.router.navigateByUrl("");
    return this.http.post(`${this.baseUrl}/compras`, data).subscribe();
  }

  deleteVenta(id:any){
return this.http.delete(`${this.baseUrl}/compras/${id}`).subscribe()
  }
}


