import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators, FormControl} from '@angular/forms';
import { ProductosService } from '../../services/productos.service';
import {Router} from '@angular/router'
import { ComprasService } from '../../services/compras.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
token:any;
formProduct:any;
data:any;
productosList:any;
status:string ='table';
idToEdit = 0;
userName:any;
user = 0;
nivel:any;
compraslog:any;
constructor(private fb: FormBuilder, private ps: ProductosService, private router: Router,private cs: ComprasService) { }

  ngOnInit(): void {
    this.createForm();
    if(JSON.parse(localStorage.getItem('user') || '')){
      this.user = 1;
      this.userName = JSON.parse(localStorage.getItem('user') || '');
      this.nivel = this.userName.nivel;
      this.cs.getCompras(this.userName.id).subscribe((data:any) => {
        this.compraslog = data;
      });

    }
  }

createForm(){
  this.formProduct = this.fb.group({
    descripcion:['',Validators.required],
    precio:['',Validators.required],
    id_departamento:['',Validators.required]
  })

  this.refreshProductos();


}


refreshProductos(){
return this.ps.getProductos().subscribe( (data:any) => {
  this.productosList = data;
} )

}

changeStatus(status:string){this.status=status;}

  logout(): void{
    this.token = '';
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("user");
  }

  save(){
    this.status = 'table';

    this.data = {
      descripcion: this.formProduct.value.descripcion,
      precio: this.formProduct.value.precio,
      id_departamento: 1,
    }
    if(this.idToEdit != 0)
    {
      this.ps.storeProdcutEdit(this.idToEdit,this.data);
    }else{
      this.ps.storeProducto(this.data);

    }

    this.router.navigateByUrl("");

  }

 
  deleteProductoSeleccionado(id:any){

    return this.ps.deleteProducto(id);

  }
  showProdcuto(id:any){
    this.status = 'add';
      this.idToEdit = id;

    return this.ps.showProductSelect(id).subscribe( (data:any) => {
      this.formProduct.get('descripcion').setValue(data.descripcion);
      this.formProduct.get('precio').setValue(data.precio);
      this.formProduct.get('id_departamento').setValue(data.id_departamento);
    } )
  }

  
}
