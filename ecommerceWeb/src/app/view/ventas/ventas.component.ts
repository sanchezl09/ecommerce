import { Component, OnInit } from '@angular/core';
import { ComprasService } from '../../services/compras.service';
import { VentaService } from '../../services/venta.service';


@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {
  token:any;
  userName:any;
  user = 0;
  nivel:any;
  compraslog:any;
  status:string ='table';
  allCompras:any
  constructor(private cs: ComprasService, private vs: VentaService) { }
  changeStatus(status:string){this.status=status;}

  ngOnInit(): void {

    this.refreshCompras();
    if(JSON.parse(localStorage.getItem('user') || '')){
      this.user = 1;
      this.userName = JSON.parse(localStorage.getItem('user') || '');
      this.nivel = this.userName.nivel;
      this.cs.getCompras(this.userName.id).subscribe((data:any) => {
        this.compraslog = data;
      });

    }
  }

  logout(): void{
    this.token = '';
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("user");
  }


  refreshCompras(){
    return this.cs.getAllCompras().subscribe( ( data:any ) => {
      this.allCompras = data;

      console.log(data);
    })
  }
showVenta(id:any){}
deleteVentaSeleccionado(id:any){

  return this.vs.deleteVenta(id);
}

}
