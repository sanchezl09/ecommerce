import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../auth-service.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private authServise: AuthServiceService,
    private router: Router,
    ) { }
token:any;
  ngOnInit(): void {
  }

  onLogin(form:any):void{
      
    this.authServise.login(form.value).subscribe( res => {
      console.log(res, 'res');
      const token:any = localStorage.getItem('ACCESS_TOKEN');
      this.router.navigateByUrl('');

    },(error: any) => {
      
    });
      
  }

 

}
