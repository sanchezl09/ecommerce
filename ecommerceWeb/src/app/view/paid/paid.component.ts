import { Component, OnInit } from '@angular/core';
import { VentaService } from '../../services/venta.service';
import { DatePipe } from '@angular/common';
import { ComprasService } from '../../services/compras.service';

@Component({
  selector: 'app-paid',
  templateUrl: './paid.component.html',
  styleUrls: ['./paid.component.css'],
  providers: [DatePipe]

})
export class PaidComponent implements OnInit {
  productos_compra = JSON.parse(localStorage.getItem('productos') || '');
  cantidad = this.productos_compra.length;
  userLog = JSON.parse(localStorage.getItem('user') || '');
  myDate = new Date();
  total = 0;
  userName:any;
user = 0;
nivel:any;
compraslog:any;
token:any;

  constructor(private vs: VentaService,private datePipe: DatePipe,private cs: ComprasService) { }

  ngOnInit(): void {
    this.productos_compra.map( ( data:any ) => {
      this.total += parseFloat(data.precio);
    })
    if(JSON.parse(localStorage.getItem('user') || '')){
      this.user = 1;
      this.userName = JSON.parse(localStorage.getItem('user') || '');
      this.nivel = this.userName.nivel;
      this.cs.getCompras(this.userName.id).subscribe((data:any) => {
        this.compraslog = data;
      });

    }
  }

  storeVenta(){

  let data = {
    importeTotal: this.total,
    fecha_venta:  this.datePipe.transform(this.myDate, 'yyyy-MM-dd'),
    cantidad_productos: this.cantidad,
    estatusEliminar: 1,
    id_user: this.userLog.id
  }

 this.vs.saveVenta(data);

}
logout(): void{
  this.token = '';
  localStorage.removeItem("ACCESS_TOKEN");
  localStorage.removeItem("user");
}

}
