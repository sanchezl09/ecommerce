import { Component, OnInit } from '@angular/core';
import { ComprasService } from '../../services/compras.service';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})
export class ComprasComponent implements OnInit {
  productos:any;
  productosSelectionados:any = [];
  token:any;
  userName:any;
  user = 0;
  nivel:any;
  compraslog:any;
  constructor(private cs: ComprasService) { }

  ngOnInit(): void {
    if(JSON.stringify(localStorage.getItem('user') || '')){
      this.user = 1;
      this.userName = JSON.stringify(localStorage.getItem('user') || '');
      this.nivel = this.userName.nivel;
      this.cs.getCompras(this.userName.id).subscribe((data:any) => {
        this.compraslog = data;
        console.log('d', )
      });

    }
  }
  logout(): void{
    this.token = '';
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("user");
  }

  
}
