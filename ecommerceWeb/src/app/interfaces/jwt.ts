export interface JwtI {
    id: number,
    nombre:string,
    user: Object,
    email: string,
    token: string,
}
