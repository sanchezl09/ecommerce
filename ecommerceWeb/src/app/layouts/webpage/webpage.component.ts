import { Component, OnInit, ViewChildren } from '@angular/core';
import { LoginComponent } from 'src/app/view/login/login.component';
import { ProductosService } from '../../services/productos.service';
import { ComprasService } from '../../services/compras.service';
import { FormGroup,  FormBuilder,  Validators, FormControl} from '@angular/forms';

import { Router } from '@angular/router';
@Component({
  selector: 'app-webpage',
  templateUrl: './webpage.component.html',
  styleUrls: ['./webpage.component.css']
})
export class WebpageComponent implements OnInit {
productos:any;
productosSelectionados:any = [];
token:any;
userName:any;
user = 0;
nivel:any;
compraslog:any;
formProduct:any;
idToEdit =0;
  constructor(private ps: ProductosService, 
    private router: Router,
    private cs: ComprasService,
    private fb: FormBuilder
  ) { }
  
  ngOnInit(): void {
    this.refreshProductos();
    if(JSON.parse(localStorage.getItem('user') || '')){
      this.user = 1;
      this.userName = JSON.parse(localStorage.getItem('user') || '');
      this.nivel = this.userName.nivel;
      this.cs.getCompras(this.userName.id).subscribe((data:any) => {
        this.compraslog = data;
      });

    }
  }
  
 
saveProducto(producto:any){
  this.productosSelectionados.push(producto);
  localStorage.setItem('productos', JSON.stringify(this.productosSelectionados));

}

  refreshProductos(){
    return this.ps.getProductos().subscribe( (data:any ) => {
      console.log('prod', data);
      this.productos = data;
    });
  }
  logout(): void{
    this.token = '';
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("user");
  }
 
  showProduct(id:any){
    this.idToEdit = id;

  
    this.router.navigateByUrl('productos');
  }

}
