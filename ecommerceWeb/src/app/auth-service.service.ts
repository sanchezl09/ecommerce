import { Injectable } from '@angular/core';
import { BehaviorSubject, observable, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { tap } from 'rxjs/operators';
import { UserI } from './interfaces/user';
import { JwtI } from './interfaces/jwt';
import { environment } from '../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  baseUrl = environment.appUrl;
  authSubject= new BehaviorSubject(false);
   private token: string;
   username:any={};
   userLog:any= "";
   nameuser:any={};
  constructor(private httpc:HttpClient, private jwtHelper: JwtHelperService ) { 
    this.token = '';
  }
  login(usuario: UserI): Observable<JwtI>{

    return this.httpc.post<JwtI>(`${this.baseUrl}/auth/signin`, usuario).pipe(tap(
      (res: JwtI) => {
          if(res){
            console.log('dsada',res);
            this.saveToken(res.token, JSON.stringify(res.user));
          }
      }
    ))
  }

  private saveToken(token: string, usuario: string ): void{
    console.log('dasda', usuario);
    localStorage.setItem('ACCESS_TOKEN', token);
    localStorage.setItem('user', usuario);
    this.token = token;
  }
}



