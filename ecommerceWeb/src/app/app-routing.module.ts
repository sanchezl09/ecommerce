import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AuthComponent } from './layouts/auth/auth.component';
import { WebpageComponent } from './layouts/webpage/webpage.component';

import { LoginComponent } from "./view/login/login.component";
import { CartComponent } from "./view/cart/cart.component";
import { PaidComponent } from "./view/paid/paid.component";
import { ProductosComponent } from "./view/productos/productos.component";
import { ComprasComponent } from "./view/compras/compras.component";
import { VentasComponent } from "./view/ventas/ventas.component";






const routes: Routes = [
  {path: "", component: WebpageComponent },
  {path: "cart", component: CartComponent },
  {path: "paid", component: PaidComponent },
  {path: "productos", component: ProductosComponent },
  {path: "compras", component: ComprasComponent },
  {path: "ventas", component: VentasComponent },






  {
    path: "auth",
    component: AuthComponent,
    children: [
      { path: "login", component: LoginComponent },
      { path: "", redirectTo: "login", pathMatch: "full" },
    ],
  }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
